#quiz game

## welcome the user
print("Welcome to the quiz user")
##ask the user if they want to play
playing = input("Do you want to play the game? \n")
score = 0
## if they input anything other than yes, the program quits
if playing != 'yes':
    print("Game ended")
    quit()
#if they entered yes, the quiz will begin
print("The quiz is beginning")

answer1 = int(input("What number comes first? 1 or 2 \n"))
if answer1 != 1:
    print("Wrong answer!")
else:
    print("Correct!")
    score += 1

answer2 = input("How do you spell quiz? quiz or quizz \n")
if answer2 != "quiz":
    print("Wrong answer!")
else:
    print("Correct!")
    score += 1

answer3 = int(input("What number comes first? 10 or 20 \n"))
if answer3 != 10:
    print("Wrong answer!")
else:
    print("Correct!")
    score += 1


answer4 = input("What language is this? python or js \n")
if answer4 != "python":
    print("Wrong answer!")
else:
    print("Correct!")
    score += 1


print(f"Quiz is over\n You scored {score} out of 4")


